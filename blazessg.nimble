# Package

version       = "0.1.3"
author        = "nature"
description   = "Blazebone's very own static site generator"
license       = "MIT"
srcDir        = "src"
bin           = @["blazessg"]



# Dependencies

requires "nim >= 1.2.6"
requires "markdown >= 0.8.0"
