import
  markdown

import
  htmlparser,
  options as opt,
  os,
  parseopt,
  strformat,
  strtabs,
  strutils,
  times,
  xmltree

type
  CliOptions = object
    input: string
    output: string
    lang: string

# Get the package information at compile time
const
  nimbleDump = staticExec("nimble dump ..").split("\n")
  pkgConfig = (
    name: nimbleDump[0].split(":")[1][2..^1],
    version: nimbleDump[1].split(":")[1][2..^1],
    author: nimbleDump[2].split(":")[1][2..^1],
    desc: nimbleDump[3].split(":")[1][2..^1]
  )

# Magic names
const
  blogDirName = "blog"
  customIndexName = "_index.html"
  customHeadName = "_head.html"
  customNavName = "_nav.html"

# Default values
const
  defaultHead = readFile("src/_head.html")
  defaultBlogIndex = readFile("src/_blogIndex.html")

# Initialize the default
var
  options = CliOptions(input: "src", output: "out", lang: "en")

# Get the "closest" custom defined head or default one
proc getHead(srcPath: string): XmlNode =
  if srcPath == "":
    # Return default head
    return parseHtml(defaultHead)[0]

  for kind, path in walkDir(srcPath):
    if path.extractFilename == customHeadName:
      return loadHtml(path)[0]

  getHead(srcPath.parentDir)

# Get the "closest" custom defined nav or null
proc getNav(srcPath: string): Option[XmlNode] =
  if srcPath == "":
    # Return default (none)
    return none(XmlNode)

  for kind, path in walkDir(srcPath):
    if path.extractFilename == customNavName:
      return some(loadHtml(path)[0])

  getNav(srcPath.parentDir)

# Normalize html by inject head, navigation, and wrapping with html tags
proc normalizeHtml(html: var XmlNode, srcPath: string) =
  let
    hasBody = html.tag == "body" or html.findAll("body").len == 1
    hasNav = html.findAll("nav").len == 1
    hasHead = html.findAll("head").len == 1
    hasHtml = html.tag == "html"
    head = getHead(srcPath.parentDir)
    nav = getNav(srcPath.parentDir)
    lang = 
      if srcPath.extractFilename.split(".").len == 2:
        options.lang
      else:
        srcPath.extractFilename.split(".")[^2]

  if not hasBody:
    echo $html
    raise newException(ValueError, "The HTML markup must contain a <body> tag!")

  if not hasHtml:
    let root = newElement("html")
    root.attrs = {"lang": lang}.toXmlAttributes
    root.add html
    html = root

  if not hasHead:
    html.insert(head, 0)

  if nav.isSome() and not hasNav:
    html.child("body").add get(nav)

# Utility function to write normalized html with proper DOCTYPE tag
proc writeNormalizedHtml(srcPath, outPath: string, html: var XmlNode) =
  html.normalizeHtml(srcPath)
  # Don't forget to add DOCTYPE tag
  let rawHtml = "<!DOCTYPE html>\n" & $html
  writeFile(outPath, rawHtml)

# Create an index for the blog posts
proc createBlogIndex(srcBlogPath: string, outBlogPath: string) =
  let customIndex = srcBlogPath.joinPath(customIndexName)
  
  var 
    html: XmlNode
    linkContainer: XmlNode

  if customIndex.existsFile:
    html = loadHtml(customIndex)[0]
  else:
    html = parseHtml(defaultBlogIndex)[0]

  for node in html.findAll("div"):
    if node.attrs.hasKey("class") and node.attrs["class"].contains("links"):
      linkContainer = node

  # We use the fact that file system is sorted alphabetically
  for kind, path in walkDir(srcBlogPath):
    if path.extractFilename[0] != '_' and kind == pcFile:
      let
        (_, filename, _) = path.splitFile
        date = parse(filename.split("_")[0], "yyyyMMdd").format("yyyy-MM-dd")
        title = filename.split("_", maxsplit = 1)[1]
        href = "/" & outBlogPath.tailDir.joinPath(filename & ".html")
      
      # Create the link element
      var a = newElement("a")
      a.attrs = {"href": href}.toXmlAttributes
      a.add newText(date & " - " & title)
      # Add it to the index
      linkContainer.add a

  writeNormalizedHtml(srcBlogPath.joinPath("index.html"),
                      outBlogPath.joinPath("index.html"), html)

# Walk the source and convert/copy file to populate the output dir
proc walkSource(srcPath: string) =
  for kind, path in walkDir(srcPath):
    if path.extractFilename[0] != '_':
      let outPath = path.replace(options.input, options.output)
      stdout.write &"\tGenerating {outPath}... "; stdout.flushFile()
      case kind
      of pcDir:
        createDir(outPath)
        if path.lastPathPart() == blogDirName:
          stdout.write "Generating index... "; stdout.flushFile()
          createBlogIndex(path, outPath)
        echo "Done!"
        walkSource(path)
      of pcFile:
        case path.split(".")[^1].toLower()
        of "md":
          # Convert md to html
          var html: XmlNode = parseHtml("<body>" & markdown(readFile(path)) & "</body>")
          writeNormalizedHtml(path, outPath.replace(".md", ".html"), html)
        of "html":
          var html: XmlNode = loadHtml(path)[0]
          writeNormalizedHtml(path, outPath, html)
        else:
          # Simply copy the other files to output dir
          copyFile(path, outPath)
        echo "Done!"
      else:
        copyFile(path, outPath)
        echo "Done!"

when isMainModule:
  let usage = "  BlazeSSG v" & pkgConfig.version & " - Written by " & pkgConfig.author & """
  

  """ & pkgConfig.desc & """
  

  Usage:
    blazesssg <src> [options]

  Argument:
    src                   The directory containing the sources files for the website; Default to 'src/'.
  Options:
    --output-dir=<dir>    The output dir containing the generated HTML; Default to 'out/'.
    --lang=<lang>         The default language of the site; Default to 'en'.
  """

  # Parse the args
  for kind, key, val in getopt():
    case kind
    of cmdArgument:
      options.input = normalizedPath(key)
    of cmdLongOption:
      case key
      of "output-dir":
        options.output = normalizedPath(val)
      of "lang":
        options.lang = val
      of "help":
        quit(usage, 0)
      else:
        discard
    of cmdShortOption:
      case key
      of "h":
        quit(usage, 0)
      else:
        discard
    of cmdEnd: break

  # Temporarily backup the existing output dir
  let tmpOut = options.output & "-tmp"
  if existsDir(options.output):
    stdout.write &"'{options.output}' already exists, moving it temporarily ({tmpOut})... "; stdout.flushFile()
    moveDir(options.output, tmpOut)
    echo "Done!"

  # Create output dir
  stdout.write &"Creating {options.output}... "; stdout.flushFile()
  createDir(options.output)
  echo "Done!"

  echo &"Generating site from '{options.input}'... "
  walkSource(options.input) 
  echo "Static site generation successful!"

  # Remove backup dir if everythin went well
  stdout.write "Cleaning up... "; stdout.flushFile()
  removeDir(tmpOut)
  echo "Done!"
