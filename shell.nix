{ pkgs ? import ./pkgs.nix }:

with pkgs;
let
  myPkgs = import /home/nature/nixpkgs {};
in mkShell {
  buildInputs = [
    nim-unwrapped
    myPkgs.nimlsp
    pcre
  ];
  shellHook = ''
    echo 'Entering blazessg'
    set -v

    set +v
  '';
}
