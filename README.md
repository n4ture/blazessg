# BlazeSSG

A simple and minimalist static site generator made for
[my own website](https://blazeb.one).

## Usage

```bash
Usage:
    blazesssg <src> [options]

Argument:
    src                   The directory containing the sources files for the website; Default to 'src/'.
Options:
    --output-dir=<dir>    The output dir containing the generated HTML; Default to 'out/'.
    --lang=<lang>         The default language of the site; Default to 'en'.
```

### File structure and special files

Every file or directory name starting by `_` will be ignored. Some special
files and directories will be picked up by BlazeSSG in order to provide custom behaviour.

| Name | Description |
| ---- | ----------- |
| `_head.html` | Provide custom `<head>` element to all the files in the same directory and below. |
| `_nav.html` | Provide custom `<nav>` element to provide with a common navigation for all the pages. Note that it won't override it if a `<nav>` is already present in the page. |
| `blog/` | Special directory containing all the blog post, an index will be generated there. |
| `blog/_index.html` | Provide a custom index file, it needs to contain a `<div class="links"></div>` tag, this is the placeholder that will be filled with `<a>` tag with all the blog posts. |

The `lang` attribute of the `<html>` tag can be defined by naming the file
`mypage.fr.html`. Although if the `<html>` is already defined in the file, it
won't be overriden.

#### Blog

The `blog/` directory being specifically for blog posts will generate an index
for all the files (posts) inside it. The filenames must have a specific format:
`yyyyMMdd_title.md` or `yyyyMMdd_title.html`. Where `yyyy` is the year (e.g. 
2020), `MM` the month (e.g. 11) and `dd` the day of the month (e.g. 17).
